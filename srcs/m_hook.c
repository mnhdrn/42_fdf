/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_hook.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 15:07:26 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_move_zoom(int ind, t_data *data)
{
	if (ind == 1)
		data->pix += 5;
	else if (ind == 2)
		data->pix -= 5;
	else if (ind == 3)
		data->piy += 5;
	else if (ind == 4)
		data->piy -= 5;
	else if (ind == 5 && data->margin < (WIN.win_h / 4))
		data->margin += 1;
	else if (ind == 6 && data->margin > 0)
		data->margin -= 1;
	mlx_redraw_img(data);
}

static void		ft_colors(t_data *data)
{
	int			new;

	new = data->colors_index;
	new++;
	m_change_color(data, new);
	mlx_redraw_img(data);
}

static void		ft_height(int ind, t_data *data)
{
	size_t		x;
	size_t		y;
	size_t		pos;

	x = 0;
	y = 0;
	pos = 0;
	while (y < data->mapy)
	{
		x = 0;
		while (x < data->mapx)
		{
			pos = (y * data->mapx) + x;
			if (ind == 1)
				COORD[pos].sz += (COORD[pos].act != 0) ? 2 : 0;
			else if (ind == 2)
				COORD[pos].sz -= (COORD[pos].act != 0) ? 2 : 0;
			x++;
		}
		y++;
	}
	mlx_redraw_img(data);
}

static void		ft_angle(int key, t_data *data)
{
	if (key == (0x00))
		data->angle_x += 0.05;
	if (key == (0x01))
		data->angle_y += 0.05;
	if (key == (0x02))
		data->angle_z += 0.05;
	mlx_redraw_img(data);
}

int				m_hook(int key, t_data *data)
{
	if (key == 0x35)
		m_quit(data);
	if (key == (0x7C))
		ft_move_zoom(1, data);
	if (key == (0x7B))
		ft_move_zoom(2, data);
	if (key == (0x7D))
		ft_move_zoom(3, data);
	if (key == (0x7E))
		ft_move_zoom(4, data);
	if (key == (0x18))
		ft_move_zoom(5, data);
	if (key == (0x1B))
		ft_move_zoom(6, data);
	if (key == (0x08))
		ft_colors(data);
	if (key == (0x21))
		ft_height(2, data);
	if (key == (0x1E))
		ft_height(1, data);
	if (key == (0x1D))
		m_reset(data);
	if (key == 0x00 || key == 0x01 || key == 0x02)
		ft_angle(key, data);
	return (0);
}
