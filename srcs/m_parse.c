/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_parse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:38:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/02/24 15:16:35 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void			ft_init_coord(t_data *data)
{
	size_t			i;

	data->size = (data->mapy + 1) * (data->mapx + 1);
	if (!(COORD = (t_point *)malloc(sizeof(t_point) * (data->size + 1))))
		return ;
	i = 0;
	while (i < data->size)
	{
		COORD[i].x = 0;
		COORD[i].y = 0;
		COORD[i].z = 0;
		COORD[i].sx = 0;
		COORD[i].sy = 0;
		COORD[i].sz = 0;
		COORD[i].colors = 0;
		i++;
	}
}

static void			ft_get_map_size(char *map, t_data *data)
{
	int				fd;
	char			*line;
	size_t			tmp;

	if ((fd = open(map, O_RDONLY)) == -1)
		m_error("Bad map - Failure", data);
	tmp = 0;
	while (get_next_line(fd, &line) > 0)
	{
		data->mapx = ft_countword(line, ' ');
		if (tmp == 0)
			tmp = data->mapx;
		if (data->mapx != tmp && line)
			m_error("Bad map - Failure", data);
		data->mapy++;
		(line) ? ft_strdel(&line) : 0;
	}
	if (data->mapy == 0 || data->mapx == 0)
		m_error("Bad Map - Failure", data);
	(line) ? ft_strdel(&line) : 0;
	close(fd);
}

static void			ft_get_point(char *point, t_data *data, size_t y, size_t x)
{
	char			*ret;
	size_t			pos;

	ret = NULL;
	pos = (y * data->mapx) + x;
	if (pos < data->size)
	{
		COORD[pos].sx = (double)x;
		COORD[pos].sy = (double)y;
		COORD[pos].sz = (double)ft_atoi(point);
		COORD[pos].act = ft_atoi(point);
		COORD[pos].colors = data->base_colors[data->colors_index];
	}
	if (ft_strstr(point, ",0x") != NULL)
	{
		ret = ft_strchr(point, 'x');
		if (ret)
		{
			ret++;
			data->coord[pos].colors = ft_atoi_base(ret, 16);
		}
	}
}

static void			ft_get_coord(char *map, t_data *data, size_t y, size_t x)
{
	int				fd;
	char			*line;
	char			**tab;

	if ((fd = open(map, O_RDONLY)) == -1)
		return ;
	while (get_next_line(fd, &line) > 0 && y < data->mapy)
	{
		tab = ft_strsplit(line, ' ');
		x = 0;
		while (tab[x] && x < data->mapx)
		{
			if (ft_strequ(tab[x], "") || m_countnumber(tab[x]) == 0)
				m_error("Unvalid Files", data);
			ft_get_point(tab[x], data, y, x);
			x++;
		}
		y++;
		(line) ? ft_strdel(&line) : 0;
		(tab) ? ft_str_tabdel(&tab) : 0;
	}
	(line) ? ft_strdel(&line) : 0;
	close(fd);
}

void				m_parse(char *map, t_data *data)
{
	ft_get_map_size(map, data);
	ft_init_coord(data);
	ft_get_coord(map, data, 0, 0);
}
