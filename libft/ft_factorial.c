/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factorial.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:34 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_factorial(size_t nb)
{
	int			result;

	if (nb == 0 || nb == 1)
		return (1);
	else if (nb >= 2 && nb < 13)
		result = (nb * ft_factorial(nb - 1));
	else
		return (0);
	return (result);
}
