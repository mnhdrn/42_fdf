/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:41 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:41 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strcat(char *dest, const char *src)
{
	int		i;
	int		len;

	i = 0;
	len = (int)ft_strlen(dest);
	while (*(src + i))
	{
		*(dest + (len + i)) = *(src + i);
		i++;
	}
	*(dest + (len + i)) = '\0';
	return (dest);
}
